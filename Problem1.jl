### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 4d283982-24b8-4f9e-9a90-249d796f9412
begin
	using Graphs
	using GraphPlot
	using Colors
	using PlutoUI
	using MarkdownLiteral: @mdx
	using HypertextLiteral
end

# ╔═╡ ff033cba-3ef7-4ccb-891a-9d833114f103
# begin
# 	g=SimpleGraph(4)
# 	add_edge!(g, 1, 2)
# 	add_edge!(g, 1, 3)
# 	add_edge!(g, 2, 1)
# 	add_edge!(g, 2, 4)
# 	add_edge!(g, 3, 1)
# 	add_edge!(g, 3, 4)
# 	add_edge!(g, 4, 2)
# 	add_edge!(g, 4, 3)
# 	# just plot it
# 	# gplot(g) 
# 	xx_locs=[1,2, 1, 2]
# 	yy_locs=[1, 1, 2, 2]
# 	# locs_x, locs_y = spring_layout(g)
# 	gplot(g, xx_locs, yy_locs)
# end

# ╔═╡ fbfa9ee2-958c-4463-bdc8-6aa3e4f061c4
function officesFields(rows, cols)
	PlutoUI.combine() do Child
		htlStrs=[
			[
				@htl("<li>$(string(cols * row - (cols - col))): $(Child(string(cols * row - (cols - col)), NumberField(0:10000)))</li>")
				for col in 1:cols
			]
			for row in 1:rows
		]
		
		@htl("""
		<h6>Enter packages per office:</h6>
		<ul>
		$(htlStrs)
		</ul>
		""")
	end
end

# ╔═╡ 5bceb1bf-7972-485f-9cd1-409717220500



# ╔═╡ fd6b1be4-cbf4-42a8-8231-b6928c5d9af7
md"""
### Enter building details
num of stories: $(@bind stories NumberField(1:10000))

num of offices per story: $(@bind offices_per_story NumberField(1:10000))

starting office number: $(@bind starting_office NumberField(1:10000))

###### NB!! Re-run all cells below this one if you changed the values above
"""

# ╔═╡ 9ab73af7-7ee8-43f7-87d1-21140b440258
begin
	println(stories)
	println(offices_per_story)
end

# ╔═╡ 40222d54-84b9-4cd3-811a-df27f358dc7e


# ╔═╡ fd680849-8c7b-4b91-8a91-73aba298e3cc
@bind packages_per_office officesFields(stories, offices_per_story)

# ╔═╡ 1212ffaf-7ea3-4d85-8bdc-c2473adeacd6
packages_per_office

# ╔═╡ ea5e86c0-a9e2-4446-8363-305c3427ad83
total_packages = sum(packages_per_office)

# ╔═╡ 62b78645-421b-46ca-99b4-d69ef6eeebea


# ╔═╡ f71abbda-8ddc-4786-8404-0f571dbcf4d1


# ╔═╡ 1aa04f1c-ba9f-11ec-296a-f9b8177bf1c5
md"""
### Struct definitions
"""

# ╔═╡ 66b7cc55-9d51-43ea-a5b4-0e7dac86bde7
begin
	struct Position
	    row
	    col
	end
	
	struct AdjVertex
	    pos::Position
	    weight::Int
	end
	
	mutable struct Vertex 
	    name::String
	    pos::Position
	    next::Union{Vertex,Nothing}
	    visited::Bool
	    adj_nodes::Array{AdjVertex}
	
	    num_of_packages::Int
	    h_cost::Float64
	    cost_from_src::Float64
	end
end

# ╔═╡ 25cc943a-97cf-42ec-820b-e5f96f74ece0


# ╔═╡ 2da29052-ef42-4510-9d42-e98d0b722704


# ╔═╡ 168caefe-9a0c-40ad-b1da-fa2d48f98f79
md"""
### Build grid

builds graph to be used for in the algorithm as well as a graph for visualization
"""

# ╔═╡ 80951509-18d1-47c2-b601-ef6ba7e44fa6
md"""
##### NB!!!  Always run the cell below when changing the configuration of the problem above
"""

# ╔═╡ 108e6475-3c80-416d-9bbc-935010a54b77
begin

	base_representation_graph = SimpleGraph(stories * offices_per_story)
	
	function buildGrid(rows, cols)
	    grid = [Vertex(string(j), Position(1, j), nothing, false, [], 0, Inf, Inf) for j in 1:cols]
	    # grid = [Vertex(string(j), Position(1, j), nothing, false, [], 0, 0, 0) for j in 1:cols]
	    grid = reshape(grid, 1, length(grid))
	
	    adjustment_counter = cols
	    for i in 2:rows
	
	        # create array of matrices
	        row::Array{Vertex} = [Vertex(string(j + adjustment_counter), Position(i, j), nothing, false, [], 0, Inf, Inf) for j in 1:cols] 
	        # row::Array{Vertex} = [Vertex(string(j + adjustment_counter), Position(i, j), nothing, false, [], 0, 0, 0) for j in 1:cols] 
	
	        # convert to matrix
	        row = reshape(row, 1, length(row)) #
	
	        # vertically concat to grid
	        grid = vcat(grid, row)  
	
	        adjustment_counter += cols
	
	    end
	    return grid
	end
	
	grid = buildGrid(stories, offices_per_story);
end

# ╔═╡ 20efd568-2e88-4db8-8e92-5b4ed2f7eb60
function showGraph(path)
		l = 1 : (stories * offices_per_story)
		x_locs::Vector{Int64} = []
		y_locs::Vector{Int64} = []

		n_colors=[n in path ? colorant"orange" : colorant"lightseagreen" for n in 1:(stories * offices_per_story)]

		
		for i in 1:stories
			x_locs = [x_locs; [x for x in 1:offices_per_story]]
			y_locs = [y_locs; [i for x in 1:offices_per_story]]
		end

		println(x_locs)
		println(y_locs )
		
		gplot(base_representation_graph, x_locs, y_locs, nodelabel = l, nodefillc = n_colors)#, x_locs, y_locs)
end

# ╔═╡ a579e678-3713-4fcb-b358-44d8384e9be3
showGraph([])

# ╔═╡ f2208305-2d05-4832-ac33-bd05c57a8df4
md"""
### Create edges
"""

# ╔═╡ cc4edab0-fac9-4bb7-a698-7f587d8e40a0
begin
	function createEdges()
	
	    # get total rows and cols
	    total_rows, total_cols = size(grid)
	
	    for i in 1:total_rows
	        for j in 1:total_cols
	
	            # get ref to vertex
	            vert = grid[i, j]
	
	            # check for Neighbors above
	            if (i - 1) > 0
	                push!(vert.adj_nodes, AdjVertex(Position(i - 1, j), 1))

					add_edge!(base_representation_graph, parse(Int, grid[i, j].name), parse(Int, grid[i - 1, j].name))
	            end
	
	            # check for Neighbors below
	            if (i + 1) <= total_rows
	                push!(vert.adj_nodes, AdjVertex(Position(i + 1, j), 1))

					add_edge!(base_representation_graph, parse(Int, grid[i, j].name), parse(Int, grid[i + 1, j].name))
	            end
	
	            # check for Neighbors to left
	            if (j - 1) > 0
	                push!(vert.adj_nodes, AdjVertex(Position(i, j - 1), 2))

					add_edge!(base_representation_graph, parse(Int, grid[i, j].name), parse(Int, grid[i, j - 1].name))
	            end
	
	            # check for Neighbors to right
	            if (j + 1) <= total_cols
	                push!(vert.adj_nodes, AdjVertex(Position(i, j + 1), 2))

					add_edge!(base_representation_graph, parse(Int, grid[i, j].name), parse(Int, grid[i, j + 1].name))
	            end
	        end
	    end
	end
	createEdges()
	
end

# ╔═╡ 863e4d36-0d4c-4502-830c-e8243b81a514
grid

# ╔═╡ 7595b172-4677-40f6-86bb-de2e1623cf53


# ╔═╡ 258df5f4-02ac-4d11-a525-3b05605ab575


# ╔═╡ 0eade4b1-ba6f-4bd4-bec8-d9e67640350d


# ╔═╡ f1f79b01-fcc2-4d4c-8c8e-318263fb2202


# ╔═╡ 564c9ddb-e338-4309-b858-fd3429210869


# ╔═╡ f76c05ba-560a-4a72-92fe-d95321f3ccf4
md"""
### set num of packages in offices
"""

# ╔═╡ c1ee1058-9908-4706-b151-aa96fad1236c
begin
	function setNumOfPackages()
		for j in 1:size(grid)[1]
			for i in 1:size(grid)[2]
				grid[j, i].num_of_packages = packages_per_office[(offices_per_story * j - (offices_per_story - i))]
			end
		end
	end
	setNumOfPackages()
end

# ╔═╡ 74c14c21-a00c-4495-88e8-405c2f6e78e3
grid

# ╔═╡ 57f1d986-8321-4d2c-9c80-59fb1cc3cbb4
md"""
### Define cost and heuristic functions
"""

# ╔═╡ 7c72be2f-9350-422a-b293-9b2368c86d3e
begin
	cost(w::Union{Float64,Int64}, packages::Int) = w - (5 * packages)
	# h(packages::Int) = (total_packages * 3) - (3 * packages)
	h(packages::Int) = -(3 * packages) #- (packages==0 ? 0 : 1)
	hCost(c_val, packages) = c_val + h(packages)
	
	function minCost(v_list::Array{Vertex})
	    lowest = Inf
	    lowest_index = 0
	    for i in 1:length(v_list)
	        if v_list[i].h_cost < lowest
	            lowest = v_list[i].h_cost
	            lowest_index = i
	        end
	    end
		println("minCost: all costs: ", [v.h_cost for v in v_list])
	    return lowest_index
	end
end

# ╔═╡ 298db6ba-34f2-49f6-90fd-2e471b79ab52
md"""
### Define A* search function
"""

# ╔═╡ 93f17bd5-0fb5-4a85-85c0-30092dfc986a
function findPath(current_vertex::Vertex, packages_left)::Bool

    # set vertex to visited
    current_vertex.visited = true

    # check for goal
    packages_left -= current_vertex.num_of_packages
    if packages_left == 0
        return true
    end

    # add adj verts to open list when not visited
    open_list::Array{Vertex} = []
    for adj_vert in current_vertex.adj_nodes
        vert = grid[adj_vert.pos.row, adj_vert.pos.col]
        if !vert.visited
            
            # update cost
            cost_from_src = cost(current_vertex.cost_from_src + adj_vert.weight, vert.num_of_packages)
            h_cost = hCost(cost_from_src, vert.num_of_packages)
            if h_cost < vert.h_cost
                vert.cost_from_src = cost_from_src
                vert.h_cost = h_cost
            end

            push!(open_list, vert)
        end
    end

    println("open nodes: ", [n.name for n in open_list])

    # search for lowest cost vertex to proceed to
    while (true)
        lowest_index = minCost(open_list) 

        println("lowest_index: ", lowest_index)
        
        # continue search for goal
        if lowest_index > 0
            
            # if goal reached, set next pointer and return true
            if findPath(open_list[lowest_index], packages_left)
                current_vertex.next = open_list[lowest_index]
                return true
            end

            # remove vert from list to search
            popat!(open_list, lowest_index)

        else # goal is unreachable or does not exist
            
            # set current vertex to false incase another branch has to traverse it
            current_vertex.visited = false

            return false
        end
    end
end

# ╔═╡ e4dddd19-80cc-4696-a403-fc0dd2d0c6b4
md"""
### Perform search
"""

# ╔═╡ 90c724e8-6237-44d9-9386-33c63b70a1bd
showGraph([])

# ╔═╡ 97eeb408-11dc-40ed-b399-18899e8bc7a0
begin
	start_index=findfirst(n->n.name==string(starting_office),grid)
	start_vert = grid[start_index[1], start_index[2]]
	start_vert.cost_from_src = cost(0, start_vert.num_of_packages)
	start_vert.h_cost = hCost(start_vert.cost_from_src, start_vert.num_of_packages)
	
	packages = total_packages
	
	findPath(start_vert, total_packages)
end

# ╔═╡ ad0c17da-6253-473d-b4f8-f20c6d8d2eee



# ╔═╡ 6c81e316-e5f1-40f0-9472-a246996985f9
md"""
### Print Path
"""

# ╔═╡ ba991ecb-a60b-402d-8f46-fd11a8b6c99c
begin

	path=[]
	
	function printPath(vert::Union{Vertex,Nothing})
	    if !isnothing(vert)
	        println(vert.name)
	        printPath(vert.next)
			push!(path, parse(Int64,vert.name))
	    end
	end 
	
	println("Path to goal:")
	empty!(path)
	printPath(start_vert)
end

# ╔═╡ 95aab3d3-8ab3-46cd-9c4c-8165753d1891
showGraph(path)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Colors = "5ae59095-9a9b-59fe-a467-6f913c188581"
GraphPlot = "a2cc645c-3eea-5389-862e-a155d0052231"
Graphs = "86223c79-3864-5bf0-83f7-82e725a168b6"
HypertextLiteral = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
MarkdownLiteral = "736d6165-7244-6769-4267-6b50796e6954"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
Colors = "~0.12.8"
GraphPlot = "~0.5.0"
Graphs = "~1.6.0"
HypertextLiteral = "~0.9.3"
MarkdownLiteral = "~0.1.1"
PlutoUI = "~0.7.23"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.ArnoldiMethod]]
deps = ["LinearAlgebra", "Random", "StaticArrays"]
git-tree-sha1 = "f87e559f87a45bece9c9ed97458d3afe98b1ebb9"
uuid = "ec485272-7323-5ecc-a04f-4719b315124d"
version = "0.1.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "32a2b8af383f11cbb65803883837a149d10dfe8a"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.10.12"

[[deps.Colors]]
deps = ["ColorTypes", "FixedPointNumbers", "Reexport"]
git-tree-sha1 = "417b0ed7b8b838aa6ca0a87aadf1bb9eb111ce40"
uuid = "5ae59095-9a9b-59fe-a467-6f913c188581"
version = "0.12.8"

[[deps.CommonMark]]
deps = ["Crayons", "JSON", "URIs"]
git-tree-sha1 = "4cd7063c9bdebdbd55ede1af70f3c2f48fab4215"
uuid = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
version = "0.8.6"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Compose]]
deps = ["Base64", "Colors", "DataStructures", "Dates", "IterTools", "JSON", "LinearAlgebra", "Measures", "Printf", "Random", "Requires", "Statistics", "UUIDs"]
git-tree-sha1 = "9a2695195199f4f20b94898c8a8ac72609e165a4"
uuid = "a81c6b42-2e10-5240-aca2-a61377ecd94b"
version = "0.9.3"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.GraphPlot]]
deps = ["ArnoldiMethod", "ColorTypes", "Colors", "Compose", "DelimitedFiles", "Graphs", "LinearAlgebra", "Random", "SparseArrays"]
git-tree-sha1 = "5e51d9d9134ebcfc556b82428521fe92f709e512"
uuid = "a2cc645c-3eea-5389-862e-a155d0052231"
version = "0.5.0"

[[deps.Graphs]]
deps = ["ArnoldiMethod", "Compat", "DataStructures", "Distributed", "Inflate", "LinearAlgebra", "Random", "SharedArrays", "SimpleTraits", "SparseArrays", "Statistics"]
git-tree-sha1 = "57c021de207e234108a6f1454003120a1bf350c4"
uuid = "86223c79-3864-5bf0-83f7-82e725a168b6"
version = "1.6.0"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.Inflate]]
git-tree-sha1 = "f5fc07d4e706b84f72d54eedcc1c13d92fb0871c"
uuid = "d25df0c9-e2be-5dd7-82c8-3ad0b3e990b9"
version = "0.1.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IterTools]]
git-tree-sha1 = "fa6287a4469f5e048d763df38279ee729fbd44e5"
uuid = "c8e1da08-722c-5040-9ed9-7db0dc04731e"
version = "1.4.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MarkdownLiteral]]
deps = ["CommonMark", "HypertextLiteral"]
git-tree-sha1 = "0d3fa2dd374934b62ee16a4721fe68c418b92899"
uuid = "736d6165-7244-6769-4267-6b50796e6954"
version = "0.1.1"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Measures]]
git-tree-sha1 = "e498ddeee6f9fdb4551ce855a46f54dbd900245f"
uuid = "442fdcdd-2543-5da2-b0f3-8c86c306513e"
version = "0.3.1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "5152abbdab6488d5eec6a01029ca6697dff4ec8f"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.23"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.SimpleTraits]]
deps = ["InteractiveUtils", "MacroTools"]
git-tree-sha1 = "5d7e3f4e11935503d3ecaf7186eac40602e7d231"
uuid = "699a6c99-e7fa-54fc-8d76-47d257e15c1d"
version = "0.9.4"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "cd56bf18ed715e8b09f06ef8c6b781e6cdc49911"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.4.4"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═4d283982-24b8-4f9e-9a90-249d796f9412
# ╠═ff033cba-3ef7-4ccb-891a-9d833114f103
# ╠═20efd568-2e88-4db8-8e92-5b4ed2f7eb60
# ╠═fbfa9ee2-958c-4463-bdc8-6aa3e4f061c4
# ╠═5bceb1bf-7972-485f-9cd1-409717220500
# ╠═fd6b1be4-cbf4-42a8-8231-b6928c5d9af7
# ╠═a579e678-3713-4fcb-b358-44d8384e9be3
# ╠═9ab73af7-7ee8-43f7-87d1-21140b440258
# ╠═40222d54-84b9-4cd3-811a-df27f358dc7e
# ╠═fd680849-8c7b-4b91-8a91-73aba298e3cc
# ╠═1212ffaf-7ea3-4d85-8bdc-c2473adeacd6
# ╠═ea5e86c0-a9e2-4446-8363-305c3427ad83
# ╠═62b78645-421b-46ca-99b4-d69ef6eeebea
# ╠═f71abbda-8ddc-4786-8404-0f571dbcf4d1
# ╠═1aa04f1c-ba9f-11ec-296a-f9b8177bf1c5
# ╠═66b7cc55-9d51-43ea-a5b4-0e7dac86bde7
# ╠═25cc943a-97cf-42ec-820b-e5f96f74ece0
# ╠═2da29052-ef42-4510-9d42-e98d0b722704
# ╠═168caefe-9a0c-40ad-b1da-fa2d48f98f79
# ╠═80951509-18d1-47c2-b601-ef6ba7e44fa6
# ╠═108e6475-3c80-416d-9bbc-935010a54b77
# ╠═f2208305-2d05-4832-ac33-bd05c57a8df4
# ╠═cc4edab0-fac9-4bb7-a698-7f587d8e40a0
# ╠═863e4d36-0d4c-4502-830c-e8243b81a514
# ╠═7595b172-4677-40f6-86bb-de2e1623cf53
# ╠═258df5f4-02ac-4d11-a525-3b05605ab575
# ╠═0eade4b1-ba6f-4bd4-bec8-d9e67640350d
# ╠═f1f79b01-fcc2-4d4c-8c8e-318263fb2202
# ╠═564c9ddb-e338-4309-b858-fd3429210869
# ╠═f76c05ba-560a-4a72-92fe-d95321f3ccf4
# ╠═c1ee1058-9908-4706-b151-aa96fad1236c
# ╠═74c14c21-a00c-4495-88e8-405c2f6e78e3
# ╠═57f1d986-8321-4d2c-9c80-59fb1cc3cbb4
# ╠═7c72be2f-9350-422a-b293-9b2368c86d3e
# ╠═298db6ba-34f2-49f6-90fd-2e471b79ab52
# ╠═93f17bd5-0fb5-4a85-85c0-30092dfc986a
# ╠═e4dddd19-80cc-4696-a403-fc0dd2d0c6b4
# ╠═90c724e8-6237-44d9-9386-33c63b70a1bd
# ╠═97eeb408-11dc-40ed-b399-18899e8bc7a0
# ╠═ad0c17da-6253-473d-b4f8-f20c6d8d2eee
# ╠═6c81e316-e5f1-40f0-9472-a246996985f9
# ╠═ba991ecb-a60b-402d-8f46-fd11a8b6c99c
# ╠═95aab3d3-8ab3-46cd-9c4c-8165753d1891
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
