### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 000bd5c1-78ba-47e0-894f-806cae367487
begin
	using Graphs
	using GraphPlot
	using Colors
	using PlutoUI
	using MarkdownLiteral: @mdx
	using HypertextLiteral
end

# ╔═╡ be34038c-ad74-4a58-9816-51d045dfabec


# ╔═╡ 355fe9b3-5e20-4631-9dd0-12a7cc618da6
begin
	mutable struct Signature
	    input_type::String
	    output_type::String
	end
	
	mutable struct ConstraintValues
	    min_response_time::Int
	    max_latency::Int
	    min_throughput::Int
	    min_success_rate::Float32
	end
	
	mutable struct PerformanceValues
	    response_time::Int
	    latency::Int 
	    throughput::Int
	    success_rate::Float32
	end
	
	mutable struct Action
	    name::String
	    sig::Signature
	    unary_constraints::ConstraintValues
	end
	
	mutable struct ServerLessFunction
	    name::String
	    sig::Signature
	    performance_values::PerformanceValues
	end
	
	mutable struct Node
	    action::Action
	    func::Union{ServerLessFunction,Nothing}
	    neighbours::Array{Node}
	    domain::Array{ServerLessFunction}
	end
end

# ╔═╡ 8e531c61-14aa-4b0c-8c7e-d1a774fe3504


# ╔═╡ bd5d2a24-9ceb-4822-b752-f56d1c29718f
function variableTypes(nVars)
	PlutoUI.combine() do Child
		htlStrs=[
		
			@htl("<li>A$(string(v))- </br>
				input: $(Child("inA"*string(v), TextField())) </br>
				output: $(Child("outA"*string(v), TextField())) </br>
				min_response_time: $(Child("minResA"*string(v), NumberField(1:10000))) </br>
				max_latency: $(Child("maxLatA"*string(v), NumberField(1:10000))) </br>
				min_throughput: $(Child("minThroA"*string(v), NumberField(1:10000))) </br>
				min_success_rate: $(Child("minSuccA"*string(v), NumberField(1:10000))) </br>
			</li>")
			for v in 1:nVars
		]
		
		@htl("""
		</br>
		<h6>Enter input and output types for actions:</h6>
		<ul>
		$(htlStrs)
		</ul>
		""")
	end
end

# ╔═╡ 0cda2400-d835-4160-950e-3d7fcec94d35
function functionTypes(nFuncs)
	PlutoUI.combine() do Child
		htlStrs=[
		
			@htl("<li>F$(string(f))- </br>
				input: $(Child("inF"*string(f), TextField())) </br>
				output: $(Child("outF"*string(f), TextField())) </br>
				response_time: $(Child("resF"*string(f), NumberField(1:10000))) </br>
				latency: $(Child("latF"*string(f), NumberField(1:10000))) </br>
				throughput: $(Child("throF"*string(f), NumberField(1:10000))) </br>
				success_rate: $(Child("succF"*string(f), NumberField(1:10000))) </br>
			</li>")
			for f in 1:nFuncs
		]
		
		@htl("""
		</br>
		<h6>Enter input and output types for functions:</h6>
		<ul>
		$(htlStrs)
		</ul>
		""")
	end
end

# ╔═╡ bf01718c-95a3-4053-9f79-b403db6ffa60


# ╔═╡ 8e31f7a8-063b-4a2c-8b7e-e5aeb0bb752e
function userDefinedArcs(nVars)
	PlutoUI.combine() do Child
		htlStrs=[
		
			@htl("<li>A$(string(v)) neighbours --> $([
				@htl("
				A$(string(i)): $(Child("A"*string(i)*"_$(v)", CheckBox()))
			") for i in filter(x->x!==v, [nv for nv in 1:nVars])
			]) 
			</br>
			</li>")
			for v in 1:nVars
		]
		
		@htl("""
		</br>
		<h6>Enter arcs for each action:</h6>
		<ul>
		$(htlStrs)
		</ul>
		""")
	end
end

# ╔═╡ 132cf616-9e28-4f92-8ee9-9dcef4329702


# ╔═╡ 64e9c14e-f3e9-4330-a3b3-4240f077128b
function showGraph(actions, arcs)
		l = ["$(a.action.name): $((a.func!==nothing ? a.func.name : ""))" for a in actions]

		g = SimpleGraph(length(actions))

		for arc in arcs
			add_edge!(g, parse(Int64, arc[1][2:2]), parse(Int64, arc[2][2:2])) 
		end
		
		gplot(g, nodelabel = l)#, x_locs, y_locs)
end

# ╔═╡ 91304e19-5711-413c-9ea3-fc340cc3b2b3


# ╔═╡ 5d8ab9df-94dd-4856-bd7f-cf6072d77c45
md"""
### Get constraint problem details
"""

# ╔═╡ 499c29ae-e330-405b-85bf-dcf4728af182
md"""
### Enter probelm details
num of actions: $(@bind num_of_actions NumberField(1:10000))

num of functions: $(@bind num_of_functions NumberField(1:10000))

"""

# ╔═╡ 608e9c89-4fdf-4fd9-9685-e23485780af8
md"""
#### Abstract types for use by Actions

	NUMBER
	LIST
	TREE

#### Make sure the types above are used for the actions below!


$(@bind actionDetails variableTypes(num_of_actions))
"""

# ╔═╡ 7ae9e4d5-f9ca-424d-a8af-11e01cdcd037
actionDetails

# ╔═╡ e8a851f4-7663-4c83-a4a9-b943cc716431
md"""

#### Check the boxes of the neighbours of an action that apply

###### NB!! Make sure to check boxes of both actions that share an arc. e.g if A1 and A2 share an arc, check the A2 box for A1 and check the A1 box for A2.
Not doing this will cause the algorithm to fail. For an example of how arcs are created, look for the pre-defined arcs section and notice how each action has its neighbours specified independent of what has already been defined for other actions

$(@bind arcDetails userDefinedArcs(num_of_actions))
"""

# ╔═╡ 1deb8880-68de-4e22-b782-efad3b0cf315
arcDetails

# ╔═╡ 852e1ecf-afb7-49ae-a9cc-05b7845fd393


# ╔═╡ c4d213c5-e225-4a05-a8be-136c6c77d283
md"""
#### Concrete types for use by Functions

	ARRAY
	INT
    FLOAT
    ARRAY
    LINKEDLIST
    STRING
    GRAPH
    BINARYTREE

#### Make sure the types above are used for the functions below!


$(@bind functionDetails functionTypes(num_of_functions))
"""

# ╔═╡ dff1b9f6-4de8-41c3-92b6-748cbd76c75c
functionDetails

# ╔═╡ b9526363-0adf-4ff2-a38f-64614a002a77


# ╔═╡ 7f7d2f40-10f1-4cf6-bf68-04669d0685ec
md"""
### Use user defined values? 

Check the box for yes, or leave unchecked for no:

$(@bind use_user_defined_values CheckBox())
"""

# ╔═╡ ff39e616-060e-4440-993d-b82fea72a118
use_user_defined_values

# ╔═╡ 0490160a-f259-41d2-83ef-72d5527413be


# ╔═╡ b70e2c58-e222-4f4e-8328-93e3640e0f34


# ╔═╡ 94c04974-03f3-4bad-8314-98d7f713675a
function createUserDefinedVars(funcs)::Array{Node}
	u_def_vars::Array{Node} = []

	v_count = 1
	i = 1
	while i <= length(actionDetails)
		push!(u_def_vars, Node(
			Action("A"*string(v_count), Signature(actionDetails[i], actionDetails[i+1]), ConstraintValues(actionDetails[i+2], actionDetails[i+3], actionDetails[i+4], actionDetails[i+5])),
	        nothing,
	        [],
	        funcs
		))

		i+=6
		v_count+=1
	end

	return u_def_vars
end

# ╔═╡ da12dfd0-4a26-45e1-ad9a-3140a27690e1
function createUserDefinedFuncs()
	u_def_funcs::Array{ServerLessFunction} = []

	f_count = 1
	i = 1
	while i <= length(functionDetails)
		push!(u_def_funcs, ServerLessFunction(
			"F"*string(f_count), 
			Signature(functionDetails[i], functionDetails[i+1]),
			PerformanceValues(functionDetails[i+2], functionDetails[i+3], functionDetails[i+4], functionDetails[i+5])
		))

		i+=6
		f_count+=1
	end

	return u_def_funcs	
end

# ╔═╡ d22a0e5f-dabf-42dd-b913-a4e5f0eaa826
function createUserDefinedArcs()
	u_def_arcs = []

	a_count = 1
	i = 1
	while i <= length(arcDetails)

		# only add neighbor if box was checked
		if arcDetails[i]
			neighbor = string(keys(arcDetails)[i])[1:2]
			push!(u_def_arcs, ("A$(a_count)", neighbor))
		end
		
		# there are only n-1 possible neighbours per action
		if i % (num_of_actions - 1) === 0
			a_count+=1
		end
		
		i+=1

	end

	return u_def_arcs
end

# ╔═╡ 379d4f1a-0bad-47d1-bded-fcc3c1833e6e


# ╔═╡ 73e69238-9a80-4ea0-be8e-1546b2c98c0c


# ╔═╡ 2460451b-1159-496b-a610-9f14b2917149


# ╔═╡ b4e40fa8-ca5f-43da-a625-cb218cece347
user_defined_functions = createUserDefinedFuncs()

# ╔═╡ 2ce3f2ab-5974-4c26-bc4b-994e72b23bfd
user_defined_variables = createUserDefinedVars(user_defined_functions)

# ╔═╡ 9373e06f-d98d-4405-a9a3-96caa3ca1b94
user_defined_arcs = createUserDefinedArcs()

# ╔═╡ 5112f8b8-c92e-11ec-1036-438bccfe6606
md"""
### define concrete to abstract mappings
"""

# ╔═╡ 019a6ff8-b1a6-47b6-865c-93c64b793dd9
concrete_to_abstract = Dict([
    ("INT", "NUMBER"),
    ("FLOAT", "NUMBER"),
    ("ARRAY", "LIST"),
    ("LINKEDLIST", "LIST"),
    ("STRING", "LIST"),
    ("GRAPH", "TREE"),
    ("BINARYTREE", "TREE"),
])

# ╔═╡ 15c115a2-1576-4b48-bdfb-790b4aa02aa4


# ╔═╡ ebd78c71-ed9f-4d38-8ef6-4c5da2353cb1


# ╔═╡ 01f74f66-43ed-4ad1-b86b-4d54f46f1822
md"""
### define test set of functions
"""

# ╔═╡ 6641f6ab-84b6-4392-b3ab-d806d5bdaf20
functions = [
    ServerLessFunction("G", Signature("STRING", "FLOAT"), PerformanceValues(250, 70, 150, 71)),
    ServerLessFunction("F", Signature("ARRAY", "INT"), PerformanceValues(250, 70, 150, 71)),
    ServerLessFunction("R", Signature("ARRAY", "INT"), PerformanceValues(210, 60, 150, 71)),

    ServerLessFunction("H", Signature("ARRAY", "INT"), PerformanceValues(130, 40, 400, 85)),
    ServerLessFunction("I", Signature("LINKEDLIST", "FLOAT"), PerformanceValues(130, 40, 400, 85)),
    ServerLessFunction("S", Signature("ARRAY", "INT"), PerformanceValues(110, 35, 400, 85)),

    ServerLessFunction("K", Signature("STRING", "INT"), PerformanceValues(320, 180, 60, 50)),
    ServerLessFunction("J", Signature("ARRAY", "INT"), PerformanceValues(320, 180, 60, 50)),
    ServerLessFunction("T", Signature("ARRAY", "INT"), PerformanceValues(305, 160, 60, 50)),

    ServerLessFunction("M", Signature("STRING", "INT"), PerformanceValues(40, 10, 40, 80)),
    ServerLessFunction("L", Signature("ARRAY", "INT"), PerformanceValues(40, 10, 40, 80)),
    ServerLessFunction("U", Signature("ARRAY", "INT"), PerformanceValues(32, 5, 40, 80)),
]

# ╔═╡ 78b3352d-2f0a-4ff0-9408-7a42e2bf9423
md"""
### Define test set of variables
"""

# ╔═╡ 5fd32271-f7b9-430b-95fd-77e21a84462e
variables = [
    Node(
        Action("A1", Signature("LIST", "NUMBER"), ConstraintValues(200, 80, 100, 70)),
        nothing,
        [],
        functions
    ),
    Node(
        Action("A2", Signature("LIST", "NUMBER"), ConstraintValues(100, 50, 300, 80)),
        nothing,
        [],
        functions
    ),
    Node(
        Action("A3", Signature("LIST", "NUMBER"), ConstraintValues(300, 200, 50, 40)),
        nothing,
        [],
        functions
    ),
    Node(
        Action("A4", Signature("LIST", "NUMBER"), ConstraintValues(30, 30, 20, 75)),
        nothing,
        [],
        functions
    ),
]

# ╔═╡ 81532e3b-eb0e-4556-b847-23aa32f0b296
md"""
### Define test set of arcs
"""

# ╔═╡ 18a05537-117a-4702-94a2-37c5a08e8e36
arcs = [
    ("A1", "A2"),
    ("A1", "A3"),

    ("A2", "A1"),
    ("A2", "A3"),

    ("A3", "A1"),
    ("A3", "A2"),
    ("A3", "A4"),

    ("A4", "A3")
]

# ╔═╡ a48ad6aa-6f63-45ba-8a06-7c84f4e835f4
md"""
### define function to create neighbours
"""

# ╔═╡ 6ff11791-82c5-4da7-a0f3-818f36e80321
function createNeighbors()
	# loop through either user-defined arcs or pre-defined arcs
	vars = (use_user_defined_values ? user_defined_variables : variables)
    for arc in (use_user_defined_values ? user_defined_arcs : arcs)
        node_index = findfirst(node -> node.action.name === arc[1], vars)
        node_to_add_index = findfirst(node -> node.action.name === arc[2], vars) 
        push!(vars[node_index].neighbours, vars[node_to_add_index])
    end
end

# ╔═╡ 8547a0a8-1e05-4dc8-814e-52ce3aeb291a
md"""
### create neighbours
"""

# ╔═╡ 1a53d18d-d495-46eb-974a-5789af263391
createNeighbors()

# ╔═╡ 7c017125-26f8-4a92-8234-5fcd0f5fc000
md"""
### Show constraint graph
"""

# ╔═╡ 4bc91543-8d01-46d7-a8b1-305cbcbf6e29
showGraph((use_user_defined_values ? user_defined_variables : variables), (use_user_defined_values ? user_defined_arcs : arcs))

# ╔═╡ 2c24a46f-6fc5-4398-b677-cc09278aaa20


# ╔═╡ e8b7368b-d891-47ec-befd-cf548e7587ab
md"""
### Define compatibility functions
"""

# ╔═╡ 423c6e71-5d35-481a-971d-0c5cf3bce1c5
begin
	#= 
	    IS COMPATIBLE WITH ACTION
	
	    check if serverless function is compatible with an action 
	. =#
	function isCompatibleWithAction(func_sig::Signature, action_sig::Signature)
	    return concrete_to_abstract[func_sig.input_type] == action_sig.input_type && concrete_to_abstract[func_sig.output_type] == action_sig.output_type
	end
	
	#= 
	    IS COMPATIBLE WITH FUNCTION
	
	    check if serverless function is compatible with another serverless function 
	. =#
	function isCompatibleWithFunction(func_1::Signature, func_2::Signature)
	    println("isCompatibleWithFunction sig1: ", func_1.input_type, ", ", func_1.output_type)
	    println("isCompatibleWithFunction sig2: ", func_2.input_type, ", ", func_2.output_type)
	
	    return func_1.input_type == func_2.input_type && func_1.output_type == func_2.output_type
	end
	
end

# ╔═╡ d2e0d7b0-aa20-40ac-93f7-8e4afe0e591c
md"""
### define functions to make nodes node-consistent
"""

# ╔═╡ f29466fe-cac5-4095-a2a7-986e8d15645d
begin
	function makeNodeConsistent(node::Node)
	    new_domain = []
	    u_constraints = node.action.unary_constraints
	
	    for fun in node.domain
	        p_vals = fun.performance_values
	
	        # check if unary constraints match
	        if p_vals.response_time >= u_constraints.min_response_time && p_vals.latency <= u_constraints.max_latency 
	            if p_vals.throughput >= u_constraints.min_throughput && p_vals.success_rate >= u_constraints.min_success_rate 
	
	                # check if function and action types match
	                if isCompatibleWithAction(fun.sig, node.action.sig)
	                    push!(new_domain, fun)
	                end
	                
	            end
	        end
	
	        # set new domain
	        node.domain = new_domain
	    end
	end
	
	#= 
	    MAKE ALL NODES NODE CONISTENT
	
	    makes all nodes node-consistent
	. =#
	function makeAllNodesNodeConistent()
	    for node in (use_user_defined_values ? user_defined_variables : variables)
	        makeNodeConsistent(node)
	    end
	end
end

# ╔═╡ 1f8f5c92-064a-403f-8f76-d4a3a0ade200
md"""
### make nodes node-consisten
"""

# ╔═╡ f18d5c58-9a4f-4967-a00f-690187b3d4f8
makeAllNodesNodeConistent()

# ╔═╡ 5b250ccc-b69e-44cd-9802-840e86f9ed94
md"""
### Define MRV (minimum remaining values) function
"""

# ╔═╡ 61e578e8-50f4-413d-90cd-474aa98b294b
#= 
    MRV

    returns index of unassigned node with minimum
    number domain values left
. =#
function mrv(vars::Array{Node})::Int
    min_val = Inf
    min_index = 0
    
    for i in 1:length(vars)
        if vars[i].func === nothing && length(vars[i].domain) < min_val
            min_val = length(vars[i].domain)
            min_index = i
        end
    end

    return min_index
end

# ╔═╡ 114719db-c56b-4500-a73f-b1fcce14064a
md"""
### define cost functions
"""

# ╔═╡ 7dec4fd2-0261-4fca-aa64-c8902387b8dd
begin
	#= 
	    COST
	
	    returns cost of a function
	. =#
	function cost(func::ServerLessFunction)
	    return func.performance_values.response_time + func.performance_values.latency - func.performance_values.throughput - func.performance_values.success_rate
	end
	
	#= 
	    MIN COST
	
	    returns index of func with min cost
	. =#
	function minCost(functions::Array{ServerLessFunction})
	    if isempty(functions)
	        return 0
	    end
	
	    return argmin([cost(func) for func in functions])
	end
end

# ╔═╡ d395510b-a8f4-4fde-abb7-7a9e9648101d
md"""
### Define forward checking function
"""

# ╔═╡ e9ac3ee4-3d2f-4154-95b6-6a55bedeecac
#= 
    FORWARD CHECKING

    for each neighbouring node of n, remove
    any domain values that violate constraints 
    with n and return the new array of nodes 
    with updated values
. =#
function forwardChecking(vars::Array{Node}, index::Int)::Array{Node}
    println("forward checking from node: ", vars[index].action.name)

    # loop through neighbours
    for n in vars[index].neighbours

        # loop through neighbour domain values
        i = 1
        while (i <= length(n.domain))
            # if not compatible, remove from domain
            if !isCompatibleWithFunction(vars[index].func.sig, n.domain[i].sig) 
                println("forward check: removed ", n.domain[i].name, " from domain")
                popat!(n.domain, i)
                i -= 1
            end
            i += 1
        end
    end

    println()

    # return updated nodes
    return vars
end

# ╔═╡ 63de02e8-26bf-4b77-b184-e6ca315d8176
md"""
### Define back tracking search function
"""

# ╔═╡ 3e697c92-d36b-4634-83f2-588cbff9e4d9
#= 
    BACKTRACK SEARCH

    depth first search for a solution that satisifies all constraints.
. =#
function backtrackSearch(vars::Array{Node})::Array{Node}
    
    # check if all vars assigned
    if count([n.func !== nothing for n in vars]) == length(vars)
        println("solution found!")
        return vars 
    end

    # choose unassigned variable with minimum remaining domain values (mrv)
    var_index = mrv(vars)
    println("current var: ", vars[var_index].action.name)
    println()

    # check for empty domain
    if length(vars[var_index].domain) == 0
        println("empty domain, no solution!")
        return []
    end

    #= 
        loop until solution found or domain is empty
    . =#
    while !isempty(vars[var_index].domain)

        # find min cost domain function index
        min_index = minCost(vars[var_index].domain)
        
        # assign func to var based on min_index
        vars[var_index].func = vars[var_index].domain[min_index]

        # perform forward checking
        forward_checked_vars = forwardChecking(deepcopy(vars), var_index)

        # continue search
        solution = backtrackSearch(forward_checked_vars)

        # return solution if found
        length(solution) != 0 && return solution

        # remove checked domain func from domain
        popat!(vars[var_index].domain, min_index)

        println()

    end


    #= 
        return failure if no solution found
    . =#
    return []
end

# ╔═╡ b4d0a190-9e08-4ecd-a9fa-076a5851c6c0
md"""
### perform search
"""

# ╔═╡ 79fd00c1-22a3-47a6-8b6a-1cbde6d7ecd8
solution = backtrackSearch(deepcopy((use_user_defined_values ? user_defined_variables : variables)))

# ╔═╡ e2215e94-831a-4c01-ada4-bb0f9176fd2a
md"""
### define function to print solution
"""

# ╔═╡ acda2732-eaa3-4c51-aae3-c948469bf9b2
function printVarFuncs()
    for var in solution
        println(var.action.name, ": ", var.func.name)
    end
end

# ╔═╡ b1ba790d-d5b4-4853-b0aa-507f5fa6c2ce
md"""
### print solution if found
"""

# ╔═╡ 919f7f38-0304-460b-b102-e98b71239eac
if !isempty(solution)
	printVarFuncs()
	showGraph(solution, (use_user_defined_values ? user_defined_arcs : arcs))
else
	"No solution found"
end

# ╔═╡ 773548ea-25ba-46d0-b6fe-71987fe97755


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Colors = "5ae59095-9a9b-59fe-a467-6f913c188581"
GraphPlot = "a2cc645c-3eea-5389-862e-a155d0052231"
Graphs = "86223c79-3864-5bf0-83f7-82e725a168b6"
HypertextLiteral = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
MarkdownLiteral = "736d6165-7244-6769-4267-6b50796e6954"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
Colors = "~0.12.8"
GraphPlot = "~0.5.0"
Graphs = "~1.6.0"
HypertextLiteral = "~0.9.3"
MarkdownLiteral = "~0.1.1"
PlutoUI = "~0.7.23"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.ArnoldiMethod]]
deps = ["LinearAlgebra", "Random", "StaticArrays"]
git-tree-sha1 = "f87e559f87a45bece9c9ed97458d3afe98b1ebb9"
uuid = "ec485272-7323-5ecc-a04f-4719b315124d"
version = "0.1.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "32a2b8af383f11cbb65803883837a149d10dfe8a"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.10.12"

[[deps.Colors]]
deps = ["ColorTypes", "FixedPointNumbers", "Reexport"]
git-tree-sha1 = "417b0ed7b8b838aa6ca0a87aadf1bb9eb111ce40"
uuid = "5ae59095-9a9b-59fe-a467-6f913c188581"
version = "0.12.8"

[[deps.CommonMark]]
deps = ["Crayons", "JSON", "URIs"]
git-tree-sha1 = "4cd7063c9bdebdbd55ede1af70f3c2f48fab4215"
uuid = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
version = "0.8.6"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Compose]]
deps = ["Base64", "Colors", "DataStructures", "Dates", "IterTools", "JSON", "LinearAlgebra", "Measures", "Printf", "Random", "Requires", "Statistics", "UUIDs"]
git-tree-sha1 = "9a2695195199f4f20b94898c8a8ac72609e165a4"
uuid = "a81c6b42-2e10-5240-aca2-a61377ecd94b"
version = "0.9.3"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.GraphPlot]]
deps = ["ArnoldiMethod", "ColorTypes", "Colors", "Compose", "DelimitedFiles", "Graphs", "LinearAlgebra", "Random", "SparseArrays"]
git-tree-sha1 = "5e51d9d9134ebcfc556b82428521fe92f709e512"
uuid = "a2cc645c-3eea-5389-862e-a155d0052231"
version = "0.5.0"

[[deps.Graphs]]
deps = ["ArnoldiMethod", "Compat", "DataStructures", "Distributed", "Inflate", "LinearAlgebra", "Random", "SharedArrays", "SimpleTraits", "SparseArrays", "Statistics"]
git-tree-sha1 = "57c021de207e234108a6f1454003120a1bf350c4"
uuid = "86223c79-3864-5bf0-83f7-82e725a168b6"
version = "1.6.0"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.Inflate]]
git-tree-sha1 = "f5fc07d4e706b84f72d54eedcc1c13d92fb0871c"
uuid = "d25df0c9-e2be-5dd7-82c8-3ad0b3e990b9"
version = "0.1.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IterTools]]
git-tree-sha1 = "fa6287a4469f5e048d763df38279ee729fbd44e5"
uuid = "c8e1da08-722c-5040-9ed9-7db0dc04731e"
version = "1.4.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MarkdownLiteral]]
deps = ["CommonMark", "HypertextLiteral"]
git-tree-sha1 = "0d3fa2dd374934b62ee16a4721fe68c418b92899"
uuid = "736d6165-7244-6769-4267-6b50796e6954"
version = "0.1.1"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Measures]]
git-tree-sha1 = "e498ddeee6f9fdb4551ce855a46f54dbd900245f"
uuid = "442fdcdd-2543-5da2-b0f3-8c86c306513e"
version = "0.3.1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "5152abbdab6488d5eec6a01029ca6697dff4ec8f"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.23"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.SimpleTraits]]
deps = ["InteractiveUtils", "MacroTools"]
git-tree-sha1 = "5d7e3f4e11935503d3ecaf7186eac40602e7d231"
uuid = "699a6c99-e7fa-54fc-8d76-47d257e15c1d"
version = "0.9.4"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "cd56bf18ed715e8b09f06ef8c6b781e6cdc49911"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.4.4"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═000bd5c1-78ba-47e0-894f-806cae367487
# ╠═be34038c-ad74-4a58-9816-51d045dfabec
# ╠═355fe9b3-5e20-4631-9dd0-12a7cc618da6
# ╠═8e531c61-14aa-4b0c-8c7e-d1a774fe3504
# ╠═bd5d2a24-9ceb-4822-b752-f56d1c29718f
# ╠═0cda2400-d835-4160-950e-3d7fcec94d35
# ╠═bf01718c-95a3-4053-9f79-b403db6ffa60
# ╠═8e31f7a8-063b-4a2c-8b7e-e5aeb0bb752e
# ╠═132cf616-9e28-4f92-8ee9-9dcef4329702
# ╠═64e9c14e-f3e9-4330-a3b3-4240f077128b
# ╠═91304e19-5711-413c-9ea3-fc340cc3b2b3
# ╠═5d8ab9df-94dd-4856-bd7f-cf6072d77c45
# ╠═499c29ae-e330-405b-85bf-dcf4728af182
# ╠═608e9c89-4fdf-4fd9-9685-e23485780af8
# ╠═7ae9e4d5-f9ca-424d-a8af-11e01cdcd037
# ╠═e8a851f4-7663-4c83-a4a9-b943cc716431
# ╠═1deb8880-68de-4e22-b782-efad3b0cf315
# ╠═852e1ecf-afb7-49ae-a9cc-05b7845fd393
# ╠═c4d213c5-e225-4a05-a8be-136c6c77d283
# ╠═dff1b9f6-4de8-41c3-92b6-748cbd76c75c
# ╠═b9526363-0adf-4ff2-a38f-64614a002a77
# ╠═7f7d2f40-10f1-4cf6-bf68-04669d0685ec
# ╠═ff39e616-060e-4440-993d-b82fea72a118
# ╠═0490160a-f259-41d2-83ef-72d5527413be
# ╠═b70e2c58-e222-4f4e-8328-93e3640e0f34
# ╠═94c04974-03f3-4bad-8314-98d7f713675a
# ╠═da12dfd0-4a26-45e1-ad9a-3140a27690e1
# ╠═d22a0e5f-dabf-42dd-b913-a4e5f0eaa826
# ╠═379d4f1a-0bad-47d1-bded-fcc3c1833e6e
# ╠═73e69238-9a80-4ea0-be8e-1546b2c98c0c
# ╠═2460451b-1159-496b-a610-9f14b2917149
# ╠═b4e40fa8-ca5f-43da-a625-cb218cece347
# ╠═2ce3f2ab-5974-4c26-bc4b-994e72b23bfd
# ╠═9373e06f-d98d-4405-a9a3-96caa3ca1b94
# ╠═5112f8b8-c92e-11ec-1036-438bccfe6606
# ╠═019a6ff8-b1a6-47b6-865c-93c64b793dd9
# ╠═15c115a2-1576-4b48-bdfb-790b4aa02aa4
# ╠═ebd78c71-ed9f-4d38-8ef6-4c5da2353cb1
# ╠═01f74f66-43ed-4ad1-b86b-4d54f46f1822
# ╠═6641f6ab-84b6-4392-b3ab-d806d5bdaf20
# ╠═78b3352d-2f0a-4ff0-9408-7a42e2bf9423
# ╠═5fd32271-f7b9-430b-95fd-77e21a84462e
# ╠═81532e3b-eb0e-4556-b847-23aa32f0b296
# ╠═18a05537-117a-4702-94a2-37c5a08e8e36
# ╠═a48ad6aa-6f63-45ba-8a06-7c84f4e835f4
# ╠═6ff11791-82c5-4da7-a0f3-818f36e80321
# ╠═8547a0a8-1e05-4dc8-814e-52ce3aeb291a
# ╠═1a53d18d-d495-46eb-974a-5789af263391
# ╠═7c017125-26f8-4a92-8234-5fcd0f5fc000
# ╠═4bc91543-8d01-46d7-a8b1-305cbcbf6e29
# ╠═2c24a46f-6fc5-4398-b677-cc09278aaa20
# ╠═e8b7368b-d891-47ec-befd-cf548e7587ab
# ╠═423c6e71-5d35-481a-971d-0c5cf3bce1c5
# ╠═d2e0d7b0-aa20-40ac-93f7-8e4afe0e591c
# ╠═f29466fe-cac5-4095-a2a7-986e8d15645d
# ╠═1f8f5c92-064a-403f-8f76-d4a3a0ade200
# ╠═f18d5c58-9a4f-4967-a00f-690187b3d4f8
# ╠═5b250ccc-b69e-44cd-9802-840e86f9ed94
# ╠═61e578e8-50f4-413d-90cd-474aa98b294b
# ╠═114719db-c56b-4500-a73f-b1fcce14064a
# ╠═7dec4fd2-0261-4fca-aa64-c8902387b8dd
# ╠═d395510b-a8f4-4fde-abb7-7a9e9648101d
# ╠═e9ac3ee4-3d2f-4154-95b6-6a55bedeecac
# ╠═63de02e8-26bf-4b77-b184-e6ca315d8176
# ╠═3e697c92-d36b-4634-83f2-588cbff9e4d9
# ╠═b4d0a190-9e08-4ecd-a9fa-076a5851c6c0
# ╠═79fd00c1-22a3-47a6-8b6a-1cbde6d7ecd8
# ╠═e2215e94-831a-4c01-ada4-bb0f9176fd2a
# ╠═acda2732-eaa3-4c51-aae3-c948469bf9b2
# ╠═b1ba790d-d5b4-4853-b0aa-507f5fa6c2ce
# ╠═919f7f38-0304-460b-b102-e98b71239eac
# ╠═773548ea-25ba-46d0-b6fe-71987fe97755
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
